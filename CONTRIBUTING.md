import java.util.Scanner;

public class SumaNumeros
{
    public static void main( String[] args )
    {
        int n1, n2, suma;
        int a1, a2, suma1;
        int b1, b2, suma2;
        int c1, c2, suma3;
        int d1, d2, suma4;
        int e1, e2, suma5;
        int f1, f2, suma6;
        int g1, g2, suma7;
        int h1, h2, suma8;
        int j1, j2, suma9;
        int k1, k2, suma10;
        int l1, l2, suma11;
        int m1, m2, suma12;
        int o1, o2, suma13;
        

        Scanner teclado = new Scanner( System.in );

        n1 = 976567;
        n2 = 975774;
        suma = n1 + n2;
         
        a1 = 224766;
        a2 = 246313;
        suma1 = a1 + a2;

        b1 = 510868;
        b2 = 920502;
        suma2 = b1 + b2;
         
        c1 = 778078;
        c2 = 177295;
        suma3 = c1 + c2;
        
        d1 = 373623;
        d2 = 795559;
        suma4 = d1 + d2;
         
        e1 = 357571;
        e2 = 52435;
        suma5 = e1 + e2;
        
        f1 = 854270;
        f2 = 25445;
        suma6 = f1 + f2;
         
        g1 =496179;
        g2 = 552746;
        suma7 = g1 + g2;
        
        h1 = 501260;
        h2 =  642636;
        suma8 = h1 + h2;
         
        j1 = 246079;
        j2 = 476504;
        suma9 = j1 + j2;
        
        k1 = 551903;
        k2 = 952098;
        suma10 = k1 + k2;
         
        l1 = 276154;
        l2 = 833367;
        suma11 = l1 + l2;
        
        m1 = 97745;
        m2 = 858512;
        suma12 = m1 + m2;
         
        o1 = 387872;
        o2 = 854387;
        suma13 = o1 + o2;
        
        
        System.out.println( "La suma de " + n1 + " más " + n2 + " es " + suma + "." );
        System.out.println( "La suma de " + a1 + " más " + a2 + " es " + suma1 + "." );
        System.out.println( "La suma de " + b1 + " más " + b2 + " es " + suma2 + "." );
        System.out.println( "La suma de " + c1 + " más " + c2 + " es " + suma3 + "." );
        System.out.println( "La suma de " + d1 + " más " + d2 + " es " + suma4 + "." );
        System.out.println( "La suma de " + e1 + " más " + e2 + " es " + suma5 + "." );
        System.out.println( "La suma de " + f1 + " más " + f2 + " es " + suma6 + "." );
        System.out.println( "La suma de " + g1 + " más " + g2 + " es " + suma7 + "." );
        System.out.println( "La suma de " + h1 + " más " + h2 + " es " + suma8 + "." );
        System.out.println( "La suma de " + j1 + " más " + j2 + " es " + suma9 + "." );
        System.out.println( "La suma de " + k1 + " más " + k2 + " es " + suma10 + "." );
        System.out.println( "La suma de " + l1 + " más " + l2 + " es " + suma11 + "." );
        System.out.println( "La suma de " + m1 + " más " + m2 + " es " + suma12 + "." );
        System.out.println( "La suma de " + o1 + " más " + o2 + " es " + suma13 + "." );
    }
}